<?php

require_once("config.php");

require_once("connexion.php");

require_once("getCompetitions.php");

//Récupération des équipe des championnats
function getTeamsByLeaguesId($listeChampionnatsId){
	$teams = array();
	foreach($listeChampionnatsId as $idLigue){
		$url = "https://api-football-v1.p.rapidapi.com/v2/teams/league/$idLigue";
		$response = exectCurl($url);
		if($response){
			$response_json = json_decode($response);
			foreach($response_json->api->teams as $team){
				array_push($teams, array("idEquipe"=>$team->team_id, "nomEquipe"=>$team->name, "paysEquipe"=>$team->country));
			}
		}
	}
	return $teams;
}

$teams = getTeamsByLeaguesId($listeChampionnatsId);

//Connexion
$dbh = connexion($server, $database, $username, $password);

//Insertions
for($i=0;$i<sizeof($teams);$i++){
	$sql = "INSERT INTO \"Equipe\" VALUES (".$teams[$i]['idEquipe'].",'".$teams[$i]['nomEquipe']."','".$teams[$i]['paysEquipe']."')";
	$sth = $dbh->prepare( $sql );
	$res = $sth->execute();
}