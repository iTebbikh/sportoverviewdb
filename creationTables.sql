CREATE TABLE public."Competition"
(
    "idCompetition" integer NOT NULL,
    "nomCompetition" text COLLATE pg_catalog."default",
    "saisonCompetition" integer,
    "paysCompetition" text COLLATE pg_catalog."default",
    CONSTRAINT "Competition_pkey" PRIMARY KEY ("idCompetition")
)

CREATE TABLE public."Equipe"
(
    "idEquipe" integer NOT NULL,
    "nomEquipe" text COLLATE pg_catalog."default",
    "paysEquipe" text COLLATE pg_catalog."default",
    CONSTRAINT "Equipe_pkey" PRIMARY KEY ("idEquipe")
)

CREATE TABLE public."Match"
(
    "idMatch" integer NOT NULL,
    "dateMatch" timestamp with time zone,
    "idEquipeDomicileMatch" integer,
    "idEquipeExterieurMatch" integer,
    "roundMatch" integer,
    "idCompetition" integer,
    CONSTRAINT "Match_pkey" PRIMARY KEY ("idMatch")
)

CREATE TABLE public."Score"
(
    "idScore" integer NOT NULL,
    "butDomicileScore" integer,
    "butExterieurScore" integer,
    "idMatch" integer,
    CONSTRAINT "Score_pkey" PRIMARY KEY ("idScore")
)