<?php

require_once("config.php");

require_once("connexion.php");

require_once("getCompetitions.php");

//Récupère les informations des championnats selon les pays le nom des championnats que nous voulons récupérer
function getCurrentD1ByCountryName($listePays, $listeChampionnats){
	$listeCompetitions = array();
	foreach($listePays as $pays){
		$url = "https://api-football-v1.p.rapidapi.com/v2/leagues/current/$pays";
		$response = exectCurl($url);
		if($response){
			$response_json = json_decode($response);
			foreach($response_json->api->leagues as $ligue){
				if(in_array($ligue->name, $listeChampionnats)){
					array_push($listeCompetitions, array("idCompetition"=>$ligue->league_id, 
						"nomCompetition"=>$ligue->name, 
						"saisonCompetition"=>$ligue->season, 
						"paysCompetition"=>$ligue->country
						)
					);
				}
			}
		}
	}
	return $listeCompetitions;
}

$listeCompetitions = getCurrentD1ByCountryName($listePays, $listeChampionnats);

//Connexion
$dbh = connexion($server, $database, $username, $password);

try {
	//Insertion des compétitions
	for($i=0;$i<sizeof($listeCompetitions);$i++){
		$sql = "INSERT INTO \"Competition\" VALUES (".$listeCompetitions[$i]['idCompetition'].",'".$listeCompetitions[$i]['nomCompetition']."',".$listeCompetitions[$i]['saisonCompetition'].",'".$listeCompetitions[$i]['paysCompetition']."')";
		$sth = $dbh->prepare( $sql );
		$res = $sth->execute();
	}
	
} catch (PDOException $e) {
	echo '<pre>';	
	var_dump($e);
}
