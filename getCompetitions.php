<?php

// require_once("config.php");

// require_once("connexion.php");

//Récupération de la liste des pays
function getAllCountryName(){
	$listePays = array();
	$url = "https://api-football-v1.p.rapidapi.com/v2/countries";
	$response = exectCurl($url);
	if($response){
		$response_json = json_decode($response);
		foreach($response_json->api->countries as $pays){
			array_push($listePays, $pays->country);
		}
	}
	echo "## nb pays : ".sizeof($listePays)."\n";
	return $listePays;
}

//Récupération de l'id de la saison en cours d'un championnat selon les pays fournis par l'api
function getIdCurrentD1ByCountryName($listePays, $listeSaisons){
	$listeChampionnatsId = array();
	$championnatsPrincipals = array();
	foreach($listePays as $pays){
		$url = "https://api-football-v1.p.rapidapi.com/v2/leagues/country/$pays";
		$response = exectCurl($url);
		if($response){
			$response_json = json_decode($response);
			array_push($championnatsPrincipals, $response_json->api->leagues[0]->name);
			array_push($championnatsPrincipals, $response_json->api->leagues[1]->name);
			foreach($response_json->api->leagues as $ligue){
				if(in_array($ligue->name, $championnatsPrincipals)){
					array_push($listeChampionnatsId, $ligue->league_id);
				}
			}
		}
	}
	echo "## ligues : ".sizeof($listeChampionnatsId)."\n";
	return $listeChampionnatsId;
}

//Listes des pays
$listePays = getAllCountryName();

//Listes des saisons
$listeSaisons = ["2010", "2011", "2012", "2013", "2014", "2014", "2015", "2016", "2017", "2018", "2019", "2020"];



$listeChampionnatsId = getIdCurrentD1ByCountryName($listePays, $listeSaisons);