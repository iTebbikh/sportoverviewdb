<?php

require_once("config.php");

require_once("connexion.php");

// Connexion
$dbh = connexion($server, $database, $username, $password);

// Récupération des équipes
$listeEquipes = [];
$sql = 'select * from "Equipe"';
foreach($dbh->query($sql) as $row) {
    array_push($listeEquipes, array("idEquipe"=>$row["idEquipe"], "nomEquipe"=>$row["nomEquipe"], "paysEquipe"=>$row["paysEquipe"]));
}

// Liste des combinaisons méthodes
$listeMinXButs = [0, 0, 0.5, 1.5, 0, 0, 0.5, 1.5];
$listeMaxXButs = [0.5, 1.5, 200, 200, 0.5, 1.5, 200, 200];

for($i=0;$i<sizeof($listeMinXButs);$i++){

    // Variables
    $minXButs = $listeMinXButs[$i];
    $maxXButs = $listeMaxXButs[$i];

    echo "minXButs = $minXButs ; maxXButs = $maxXButs \n"; 

    try {
        // Récupération des matchs par équipe
        foreach($listeEquipes as $equipe){
            $idEquipe = $equipe["idEquipe"];
            $nomEquipe = $equipe["nomEquipe"];
            $plusGrandeSerie = 0;
            $serieParcouru = 0;
            $serieEnCours = 0;
            $anneeSerie = 0;
            $roundEnCours = 0;
            $matchPrecedent = null;

            // Insertion des équipes
            $sql = "INSERT INTO \"StatsEquipe\" (\"idEquipe\") VALUES (".$idEquipe.")";
            $sth = $dbh->prepare( $sql );
            $res = $sth->execute();

            // Récupération de la plus grande série de match
            if($i <=3){
                $sql = 'select distinct("m"."idMatch"), "m"."dateMatch", "m"."roundMatch", ("premiereMitempsButDomicileScore" + "premiereMitempsButExterieurScore") as totalBut
                from "Match" "m" 
                INNER JOIN "Score" "s" 
                    on "m"."idMatch" = "s"."idMatch"
                INNER JOIN "Equipe" "e" 
                    on "e"."idEquipe" = "m"."idEquipeDomicileMatch" OR  "e"."idEquipe" = "m"."idEquipeExterieurMatch"
                WHERE"idEquipeDomicileMatch" = '.$idEquipe.' or "idEquipeExterieurMatch" = '.$idEquipe.'
                order by "m"."dateMatch" ';
            } else {
                $sql = 'select distinct("m"."idMatch"), "m"."dateMatch", "m"."roundMatch", ("deuxiemeMitempsButDomicileScore" + "deuxiemeMitempsButExterieurScore") as totalBut
                from "Match" "m" 
                INNER JOIN "Score" "s" 
                    on "m"."idMatch" = "s"."idMatch"
                INNER JOIN "Equipe" "e" 
                    on "e"."idEquipe" = "m"."idEquipeDomicileMatch" OR  "e"."idEquipe" = "m"."idEquipeExterieurMatch"
                WHERE"idEquipeDomicileMatch" = '.$idEquipe.' or "idEquipeExterieurMatch" = '.$idEquipe.'
                order by "m"."dateMatch" ';
            }
            foreach($dbh->query($sql) as $row) {
                $idMatch = $row["idMatch"];
                $dateMatch = $row["dateMatch"];
                $roundMatch = $row["roundMatch"];
                $totalbut = $row["totalbut"];

                // Premier match rencontré dans la BDD
                if(is_null($matchPrecedent)){
                    $matchPrecedent = $dateMatch;
                }

                if($totalbut >= $minXButs && $totalbut <= $maxXButs){ // ATTENTION : ici le nombre de but est pris en compte
                    // Si le dernier match de championnat date d'il y a un moment, on reset. Particulièrement si c'est une nouvelle saison
                    if(strtotime($matchPrecedent) >= strtotime($dateMatch . '-1 month')){
                        $serieParcouru++;
                    } else {
                        if($serieParcouru >= $plusGrandeSerie){
                            $plusGrandeSerie = $serieParcouru;
                            $anneeSerie = date('Y', strtotime($dateMatch));
                        }
                        $serieParcouru = 1;
                    }
                }else {
                    if($serieParcouru >= $plusGrandeSerie){
                        $plusGrandeSerie = $serieParcouru;
                        $anneeSerie = date('Y', strtotime($dateMatch));
                    }
                    $serieParcouru = 0;
                }
                $matchPrecedent = $dateMatch;
            }

            // Récupération de la série actuelle
            if($i <=3){
                $sql = 'select distinct("m"."idMatch"), "m"."dateMatch", "m"."roundMatch", ("premiereMitempsButDomicileScore" + "premiereMitempsButExterieurScore") as totalBut
                from "Match" "m" 
                INNER JOIN "Score" "s" 
                    on "m"."idMatch" = "s"."idMatch"
                INNER JOIN "Equipe" "e" 
                    on "e"."idEquipe" = "m"."idEquipeDomicileMatch" OR  "e"."idEquipe" = "m"."idEquipeExterieurMatch"
                WHERE ("idEquipeDomicileMatch" = '.$idEquipe.' or "idEquipeExterieurMatch" = '.$idEquipe.')
                order by "m"."dateMatch" desc';
            } else {
                $sql = 'select distinct("m"."idMatch"), "m"."dateMatch", "m"."roundMatch", ("deuxiemeMitempsButDomicileScore" + "deuxiemeMitempsButExterieurScore") as totalBut
                from "Match" "m" 
                INNER JOIN "Score" "s" 
                    on "m"."idMatch" = "s"."idMatch"
                INNER JOIN "Equipe" "e" 
                    on "e"."idEquipe" = "m"."idEquipeDomicileMatch" OR  "e"."idEquipe" = "m"."idEquipeExterieurMatch"
                WHERE ("idEquipeDomicileMatch" = '.$idEquipe.' or "idEquipeExterieurMatch" = '.$idEquipe.')
                order by "m"."dateMatch" desc';
            }
            foreach($dbh->query($sql) as $row) {
                $dateMatch = $row["dateMatch"];
                $totalbut = $row["totalbut"];
                if($totalbut >= $minXButs && $totalbut <= $maxXButs){ // ATTENTION : ici le nombre de but est pris en compte
                    if(strtotime($dateMatch) >= strtotime($matchPrecedent . '-1 month')){
                        $serieEnCours++;
                    }else{
                        break;
                    }
                } elseif ($minXButs == 3.5 && $maxXButs == 1.5){
                    if($totalbut >= $minXButs || $totalbut <= $maxXButs){
                        if(strtotime($dateMatch) >= strtotime($matchPrecedent . '-1 month')){
                            $serieEnCours++;
                        }else{
                            break;
                        }
                    } else {
                        break;
                    }
                } else {
                    break;
                }
                $matchPrecedent = $dateMatch;
            }

            if(strtotime($dateMatch) > strtotime(date('d-m-Y H:i:s') . '-1 year')){
                // echo "n°$idEquipe $nomEquipe : Plus grande série  = $plusGrandeSerie  ;  année = $anneeSerie  ;  Série actuelle = $serieEnCours \n\n";

                // Insertion des résultats // ATTENTION : ici le nombre de but est pris en compte
                if($minXButs == 0 && $maxXButs == 0.5 && $i<=3){
                    $sql = "UPDATE \"StatsEquipe\" 
                            SET \"m0.5ButsMitempsSerieRecord\" = ".$plusGrandeSerie." ,\"m0.5ButsMitempsAnneeRecord\" = ".$anneeSerie." ,\"m0.5ButsMitempsSerieActuelle\" = ".$serieEnCours." 
                            WHERE \"idEquipe\"= ".$idEquipe."";
                } elseif($minXButs == 0 && $maxXButs == 1.5 && $i<=3) {
                    $sql = "UPDATE \"StatsEquipe\" 
                            SET \"m1.5ButsMitempsSerieRecord\" = ".$plusGrandeSerie." ,\"m1.5ButsMitempsAnneeRecord\" = ".$anneeSerie." ,\"m1.5ButsMitempsSerieActuelle\" = ".$serieEnCours." 
                            WHERE \"idEquipe\"= ".$idEquipe."";
                } elseif($minXButs == 0.5 && $maxXButs == 200 && $i<=3) {
                    $sql = "UPDATE \"StatsEquipe\" 
                            SET \"p0.5ButsMitempsSerieRecord\" = ".$plusGrandeSerie." ,\"p0.5ButsMitempsAnneeRecord\" = ".$anneeSerie." ,\"p0.5ButsMitempsSerieActuelle\" = ".$serieEnCours." 
                            WHERE \"idEquipe\"= ".$idEquipe."";
                } elseif($minXButs == 1.5 && $maxXButs == 200 && $i<=3) {
                    $sql = "UPDATE \"StatsEquipe\" 
                            SET \"p1.5ButsMitempsSerieRecord\" = ".$plusGrandeSerie." ,\"p1.5ButsMitempsAnneeRecord\" = ".$anneeSerie." ,\"p1.5ButsMitempsSerieActuelle\" = ".$serieEnCours." 
                            WHERE \"idEquipe\"= ".$idEquipe."";
                } elseif($minXButs == 0 && $maxXButs == 0.5 && $i>3){
                    $sql = "UPDATE \"StatsEquipe\" 
                            SET \"m0.5Buts2emeMitempsSerieRecord\" = ".$plusGrandeSerie." ,\"m0.5Buts2emeMitempsAnneeRecord\" = ".$anneeSerie." ,\"m0.5Buts2emeMitempsSerieActuelle\" = ".$serieEnCours." 
                            WHERE \"idEquipe\"= ".$idEquipe."";
                } elseif($minXButs == 0 && $maxXButs == 1.5 && $i>3) {
                    $sql = "UPDATE \"StatsEquipe\" 
                            SET \"m1.5Buts2emeMitempsSerieRecord\" = ".$plusGrandeSerie." ,\"m1.5Buts2emeMitempsAnneeRecord\" = ".$anneeSerie." ,\"m1.5Buts2emeMitempsSerieActuelle\" = ".$serieEnCours." 
                            WHERE \"idEquipe\"= ".$idEquipe."";
                } elseif($minXButs == 0.5 && $maxXButs == 200 && $i>3) {
                    $sql = "UPDATE \"StatsEquipe\" 
                            SET \"p0.5Buts2emeMitempsSerieRecord\" = ".$plusGrandeSerie." ,\"p0.5Buts2emeMitempsAnneeRecord\" = ".$anneeSerie." ,\"p0.5Buts2emeMitempsSerieActuelle\" = ".$serieEnCours." 
                            WHERE \"idEquipe\"= ".$idEquipe."";
                } elseif($minXButs == 1.5 && $maxXButs == 200 && $i>3) {
                    $sql = "UPDATE \"StatsEquipe\" 
                            SET \"p1.5Buts2emeMitempsSerieRecord\" = ".$plusGrandeSerie." ,\"p1.5Buts2emeMitempsAnneeRecord\" = ".$anneeSerie." ,\"p1.5Buts2emeMitempsSerieActuelle\" = ".$serieEnCours." 
                            WHERE \"idEquipe\"= ".$idEquipe."";
                }
                $sth = $dbh->prepare( $sql );
                $res = $sth->execute();
            }
        }
        
    } catch (PDOException $e) {
        echo '<pre>';	
        var_dump($e);
    }
}