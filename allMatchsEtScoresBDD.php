<?php

require_once("config.php");

require_once("connexion.php");

require_once("getCompetitions.php");

//Récupération des matchs des championnats
function getFixturesByLeaguesId($listeChampionnatsId){
	$fixtures = array();
	foreach($listeChampionnatsId as $idLigue){
		$url = "https://api-football-v1.p.rapidapi.com/v2/fixtures/league/$idLigue?timezone=Europe/Paris";
		$response = exectCurl($url);
		if($response){
			$response_json = json_decode($response);
			foreach($response_json->api->fixtures as $fixture){
				// Récupération du numéro de la journée
				$round = $fixture->round;
				$arrayRound = explode("-", $round);
				$round = intval(str_replace(" ", "", $arrayRound[1]));

				// Récupération des buts par mi-temps
				$halftime = $fixture->score->halftime;
				$halftimeGoals = explode("-", $halftime);
				$fulltime = $fixture->score->fulltime;
				$fulltimeGoals = explode("-", $fulltime);
				$secondHalftimeGoals = [$fulltimeGoals[0] - $halftimeGoals[0], $fulltimeGoals[1] - $halftimeGoals[1]];
				//echo "1e mitemps : ".$halftimeGoals[0]."-".$halftimeGoals[1]." ; 2e mitemps : ".$secondHalftimeGoals[0]."-".$secondHalftimeGoals[1]." ; fulltime : ".$fulltimeGoals[0]."-".$fulltimeGoals[1]."\n\n";

				array_push($fixtures, array("idMatch"=>$fixture->fixture_id, 
						"dateMatch"=>$fixture->event_date, 
						"idEquipeDomicileMatch"=>$fixture->homeTeam->team_id, 
						"idEquipeExterieurMatch"=>$fixture->awayTeam->team_id, 
						"roundMatch"=>$round,
						"idCompetition"=>$fixture->league_id, 
						"idScore"=>$fixture->fixture_id, 
						"butDomicileScore"=>$fixture->goalsHomeTeam, 
						"butExterieurScore"=>$fixture->goalsAwayTeam,
						"premiereMitempsButDomicileScore"=>$halftimeGoals[0],
						"premiereMitempsButExterieurScore"=>$halftimeGoals[1],
						"deuxiemeMitempsButDomicileScore"=>$secondHalftimeGoals[0],
						"deuxiemeMitempsButExterieurScore"=>$secondHalftimeGoals[1]

					)
				);
			}
		}
	}
	echo "## nb matchs : ".sizeof($fixtures)."\n";
	return $fixtures;
}

$fixtures = getFixturesByLeaguesId($listeChampionnatsId);

//Connexion
$dbh = connexion($server, $database, $username, $password);

try {
	echo "## début des insertions sql \n";
	//Insertion de tous les matchs de la saison
	for($i=0;$i<sizeof($fixtures);$i++){
		$sql = "INSERT INTO \"Match\" VALUES (".$fixtures[$i]['idMatch'].",'".$fixtures[$i]['dateMatch']."',".$fixtures[$i]['idEquipeDomicileMatch'].",".$fixtures[$i]['idEquipeExterieurMatch'].",".$fixtures[$i]['roundMatch'].",".$fixtures[$i]['idCompetition'].")";
		$sth = $dbh->prepare( $sql );
		$res = $sth->execute();
	}
	
	//Insertion des Scores des matchs terminés
	for($i=0;$i<sizeof($fixtures);$i++){
        $sql = "INSERT INTO \"Score\" VALUES (".$fixtures[$i]['idScore'].",".$fixtures[$i]['butDomicileScore'].",".$fixtures[$i]['butExterieurScore'].",".$fixtures[$i]['premiereMitempsButDomicileScore'].",".$fixtures[$i]['premiereMitempsButExterieurScore'].",".$fixtures[$i]['deuxiemeMitempsButDomicileScore'].",".$fixtures[$i]['deuxiemeMitempsButExterieurScore'].",".$fixtures[$i]['idMatch'].")";
		$sth = $dbh->prepare( $sql );
		$res = $sth->execute();
		
	}
	
} catch (PDOException $e) {
	echo '<pre>';	
	var_dump($e);
}