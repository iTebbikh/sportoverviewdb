<?php

require_once("config.php");

require_once("connexion.php");

// Connexion
$dbh = connexion($server, $database, $username, $password);

// Récupération des équipes
$listeEquipes = [];
$sql = 'select * from "Equipe"';
foreach($dbh->query($sql) as $row) {
    array_push($listeEquipes, array("idEquipe"=>$row["idEquipe"], "nomEquipe"=>$row["nomEquipe"], "paysEquipe"=>$row["paysEquipe"]));
}

// Liste des combinaisons méthodes
$listeMinXButs = [0, 0, 0, 0, 0.5, 1.5, 2.5, 3.5, 3.5];
$listeMaxXButs = [1.5, 2.5, 3.5, 4.5, 200, 200, 200, 200, 1.5];

for($i=0;$i<sizeof($listeMinXButs);$i++){

    // Variables
    $minXButs = $listeMinXButs[$i];
    $maxXButs = $listeMaxXButs[$i];

    echo "minXButs = $minXButs ; maxXButs = $maxXButs \n"; 

    try {
        // Récupération des matchs par équipe
        foreach($listeEquipes as $equipe){
            $idEquipe = $equipe["idEquipe"];
            $nomEquipe = $equipe["nomEquipe"];
            $plusGrandeSerie = 0;
            $serieParcouru = 0;
            $serieEnCours = 0;
            $anneeSerie = 0;
            $roundEnCours = 0;
            $matchPrecedent = null;

            // Insertion des équipes
            $sql = "INSERT INTO \"StatsEquipe\" (\"idEquipe\") VALUES (".$idEquipe.")";
            $sth = $dbh->prepare( $sql );
            $res = $sth->execute();

            // Récupération de la plus grande série de match
            $sql = 'select distinct("m"."idMatch"), "m"."dateMatch", "m"."roundMatch", ("butDomicileScore" + "butExterieurScore") as totalBut
            from "Match" "m" 
            INNER JOIN "Score" "s" 
                on "m"."idMatch" = "s"."idMatch"
            INNER JOIN "Equipe" "e" 
                on "e"."idEquipe" = "m"."idEquipeDomicileMatch" OR  "e"."idEquipe" = "m"."idEquipeExterieurMatch"
            WHERE"idEquipeDomicileMatch" = '.$idEquipe.' or "idEquipeExterieurMatch" = '.$idEquipe.'
            order by "m"."dateMatch" ';
            foreach($dbh->query($sql) as $row) {
                $idMatch = $row["idMatch"];
                $dateMatch = $row["dateMatch"];
                $roundMatch = $row["roundMatch"];
                $totalbut = $row["totalbut"];

                // Premier match rencontré dans la BDD
                if(is_null($matchPrecedent)){
                    $matchPrecedent = $dateMatch;
                }

                if($totalbut >= $minXButs && $totalbut <= $maxXButs){ // ATTENTION : ici le nombre de but est pris en compte
                    // Si le dernier match de championnat date d'il y a un moment, on reset. Particulièrement si c'est une nouvelle saison
                    if(strtotime($matchPrecedent) >= strtotime($dateMatch . '-1 month')){
                        $serieParcouru++;
                    } else {
                        if($serieParcouru >= $plusGrandeSerie){
                            $plusGrandeSerie = $serieParcouru;
                            $anneeSerie = date('Y', strtotime($dateMatch));
                        }
                        $serieParcouru = 1;
                    }
                } elseif ($minXButs == 3.5 && $maxXButs == 1.5){ // ATTENTION : ici le nombre de but est pris en compte
                    if($totalbut >= $minXButs || $totalbut <= $maxXButs){
                        // echo "$dateMatch | $minXButs | $maxXButs | $roundMatch | $totalbut\n";
                        // Si le dernier match de championnat date d'il y a un moment, on reset. Particulièrement si c'est une nouvelle saison
                        if(strtotime($matchPrecedent) >= strtotime($dateMatch . '-1 month')){
                            $serieParcouru++;
                        } else {
                            if($serieParcouru >= $plusGrandeSerie){
                                $plusGrandeSerie = $serieParcouru;
                                $anneeSerie = date('Y', strtotime($dateMatch));
                            }
                            $serieParcouru = 1;
                        }
                    } else {
                        if($serieParcouru >= $plusGrandeSerie){
                            $plusGrandeSerie = $serieParcouru;
                            $anneeSerie = date('Y', strtotime($dateMatch));
                        }
                        $serieParcouru = 0;
                    }
                } else {
                    if($serieParcouru >= $plusGrandeSerie){
                        $plusGrandeSerie = $serieParcouru;
                        $anneeSerie = date('Y', strtotime($dateMatch));
                    }
                    $serieParcouru = 0;
                }
                $matchPrecedent = $dateMatch;
            }

            // Récupération de la série actuelle
            $sql = 'select distinct("m"."idMatch"), "m"."dateMatch", "m"."roundMatch", ("butDomicileScore" + "butExterieurScore") as totalBut
            from "Match" "m" 
            INNER JOIN "Score" "s" 
                on "m"."idMatch" = "s"."idMatch"
            INNER JOIN "Equipe" "e" 
                on "e"."idEquipe" = "m"."idEquipeDomicileMatch" OR  "e"."idEquipe" = "m"."idEquipeExterieurMatch"
            WHERE ("idEquipeDomicileMatch" = '.$idEquipe.' or "idEquipeExterieurMatch" = '.$idEquipe.')
            order by "m"."dateMatch" desc';
            foreach($dbh->query($sql) as $row) {
                $dateMatch = $row["dateMatch"];
                $totalbut = $row["totalbut"];
                if($totalbut >= $minXButs && $totalbut <= $maxXButs){ // ATTENTION : ici le nombre de but est pris en compte
                    if(strtotime($dateMatch) >= strtotime($matchPrecedent . '-1 month')){
                        $serieEnCours++;
                    }else{
                        break;
                    }
                } elseif ($minXButs == 3.5 && $maxXButs == 1.5){
                    if($totalbut >= $minXButs || $totalbut <= $maxXButs){
                        if(strtotime($dateMatch) >= strtotime($matchPrecedent . '-1 month')){
                            $serieEnCours++;
                        }else{
                            break;
                        }
                    } else {
                        break;
                    }
                } else {
                    break;
                }
                $matchPrecedent = $dateMatch;
            }

            if(strtotime($dateMatch) > strtotime(date('d-m-Y H:i:s') . '-1 year')){
                // echo "n°$idEquipe $nomEquipe : Plus grande série  = $plusGrandeSerie  ;  année = $anneeSerie  ;  Série actuelle = $serieEnCours \n\n";

                // Insertion des résultats // ATTENTION : ici le nombre de but est pris en compte
                if($minXButs == 0 && $maxXButs == 1.5){
                    $sql = "UPDATE \"StatsEquipe\" 
                            SET \"m1.5ButsSerieRecord\" = ".$plusGrandeSerie." ,\"m1.5ButsAnneeRecord\" = ".$anneeSerie." ,\"m1.5ButsSerieActuelle\" = ".$serieEnCours." 
                            WHERE \"idEquipe\"= ".$idEquipe."";
                } elseif($minXButs == 0 && $maxXButs == 2.5) {
                    $sql = "UPDATE \"StatsEquipe\" 
                            SET \"m2.5ButsSerieRecord\" = ".$plusGrandeSerie." ,\"m2.5ButsAnneeRecord\" = ".$anneeSerie." ,\"m2.5ButsSerieActuelle\" = ".$serieEnCours." 
                            WHERE \"idEquipe\"= ".$idEquipe."";
                } elseif($minXButs == 0 && $maxXButs == 3.5) {
                    $sql = "UPDATE \"StatsEquipe\" 
                            SET \"m3.5ButsSerieRecord\" = ".$plusGrandeSerie." ,\"m3.5ButsAnneeRecord\" = ".$anneeSerie." ,\"m3.5ButsSerieActuelle\" = ".$serieEnCours." 
                            WHERE \"idEquipe\"= ".$idEquipe."";
                } elseif($minXButs == 0 && $maxXButs == 4.5) {
                    $sql = "UPDATE \"StatsEquipe\" 
                            SET \"m4.5ButsSerieRecord\" = ".$plusGrandeSerie." ,\"m4.5ButsAnneeRecord\" = ".$anneeSerie." ,\"m4.5ButsSerieActuelle\" = ".$serieEnCours." 
                            WHERE \"idEquipe\"= ".$idEquipe."";
                } elseif($minXButs == 0.5 && $maxXButs == 200) {
                    $sql = "UPDATE \"StatsEquipe\" 
                            SET \"p0.5ButsSerieRecord\" = ".$plusGrandeSerie." ,\"p0.5ButsAnneeRecord\" = ".$anneeSerie." ,\"p0.5ButsSerieActuelle\" = ".$serieEnCours." 
                            WHERE \"idEquipe\"= ".$idEquipe."";
                } elseif($minXButs == 1.5 && $maxXButs == 200) {
                    $sql = "UPDATE \"StatsEquipe\" 
                            SET \"p1.5ButsSerieRecord\" = ".$plusGrandeSerie." ,\"p1.5ButsAnneeRecord\" = ".$anneeSerie." ,\"p1.5ButsSerieActuelle\" = ".$serieEnCours." 
                            WHERE \"idEquipe\"= ".$idEquipe."";
                } elseif($minXButs == 2.5 && $maxXButs == 200) {
                    $sql = "UPDATE \"StatsEquipe\" 
                            SET \"p2.5ButsSerieRecord\" = ".$plusGrandeSerie." ,\"p2.5ButsAnneeRecord\" = ".$anneeSerie." ,\"p2.5ButsSerieActuelle\" = ".$serieEnCours." 
                            WHERE \"idEquipe\"= ".$idEquipe."";
                } elseif($minXButs == 3.5 && $maxXButs == 200) {
                    $sql = "UPDATE \"StatsEquipe\" 
                            SET \"p3.5ButsSerieRecord\" = ".$plusGrandeSerie." ,\"p3.5ButsAnneeRecord\" = ".$anneeSerie." ,\"p3.5ButsSerieActuelle\" = ".$serieEnCours." 
                            WHERE \"idEquipe\"= ".$idEquipe."";
                } elseif($minXButs == 3.5 && $maxXButs == 1.5) {
                    $sql = "UPDATE \"StatsEquipe\" 
                            SET \"m1.5p3.5ButsSerieRecord\" = ".$plusGrandeSerie." ,\"m1.5p3.5ButsAnneeRecord\" = ".$anneeSerie." ,\"m1.5p3.5ButsSerieActuelle\" = ".$serieEnCours." 
                            WHERE \"idEquipe\"= ".$idEquipe."";
                } 
                $sth = $dbh->prepare( $sql );
                $res = $sth->execute();
            }
        }
        
    } catch (PDOException $e) {
        echo '<pre>';	
        var_dump($e);
    }
}