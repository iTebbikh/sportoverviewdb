select distinct ("s"."idEquipe"), "nomEquipe", "paysEquipe", "m1.5ButsSerieRecord", "m1.5ButsAnneeRecord", "m1.5ButsSerieActuelle"
from "StatsEquipe" "s"
	INNER JOIN "Equipe" "e" 
		on "e"."idEquipe" = "s"."idEquipe"
where "e"."idEquipe" IN (select "idEquipeDomicileMatch" 
							from "Match"
							where "dateMatch" > now()
								and "dateMatch" < now() + interval '1 day')
	or "e"."idEquipe" IN (select "idEquipeExterieurMatch" 
							from "Match"
							where "dateMatch" > now()
								and "dateMatch" < now() + interval '1 day')
order by "s"."m1.5ButsSerieActuelle" desc