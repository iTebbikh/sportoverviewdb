<?php

ini_set('memory_limit', '1024M');

// $nbTotalRequetesEffectuees = file_get_contents("utilisationAPI.txt");
// $nbRequetesEffetuees = $nbTotalRequetesEffectuees % 100;
// $numApiKey = intdiv($nbTotalRequetesEffectuees, 100);

function connexion($server, $database, $username, $password){
    try {
        $dbh = new PDO("pgsql:host=$server; dbname=$database; user=$username; password=$password");
        return $dbh;
        
    } catch (PDOException $e) {
        echo '<pre>';	
        var_dump($e);
    }
}


//Requetage de RapidAPI
function exectCurl($url){
	// global $nbRequetesEffetuees, $numApiKey, $nbTotalRequetesEffectuees;
	
	// $listeAPIKey = ["14f0b38968mshd9e497e9ea30f90p1ce1cejsnb33dcc2ff713", "3a6a306519mshc330c317fb426f4p1433eajsna646e1f1d347"];

	// if($numApiKey > sizeof($listeAPIKey)-1){
	// 	echo "Plus aucune clé d'API n'est disponible\n";
	// 	kill(0);
	// }

	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_SSL_VERIFYHOST => false,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
			"x-rapidapi-host: api-football-v1.p.rapidapi.com",
			"x-rapidapi-key: 14f0b38968mshd9e497e9ea30f90p1ce1cejsnb33dcc2ff713"
		),
	));
	
	$response = curl_exec($curl);
	$err = curl_error($curl);
	// $nbRequetesEffetuees++;
	// $nbTotalRequetesEffectuees++;
	// file_put_contents("utilisationAPI.txt", $nbTotalRequetesEffectuees);

	curl_close($curl);
	
	// echo "###### Requête n°$nbRequetesEffetuees effectée\n";
	// if($nbRequetesEffetuees % 30 == 0){
	// 	//echo "Limite quota 1min : j'attend 1min et je reprends\n";
	// 	//sleep(120);
	// }
	// if($nbRequetesEffetuees == 100){
	// 	echo "Changement d'api\n";
	// 	if($numApiKey == sizeof($listeAPIKey)-1){
	// 		echo "Limite des api atteinte\n";
	// 	} else {
	// 		$numApiKey++;
	// 		$nbRequetesEffetuees = 0;
	// 	}
	// }

	if ($err) {
		echo "cURL Error #:" . $err;
	} else {
		return $response;
	}

}
